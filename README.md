# scenario

## TODO

- prepare a backup plan

## Introduction

- a demo about the entire workflow/life of a web app deployed on Kubernetes
- to avoid the build step of the docker image at every change we've developped a small faas runtime
- during the demo we'll use this runtime and inject source code (function) directly to the container

## Steps

- new project > from template > from group
- select **faas-demo**
- create a first deployment (with one pos)
- show the board of deployment
- change something in the web page and add a vulnerability and bad code -> don't forget to add sast

